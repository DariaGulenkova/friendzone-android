package net.osaila.friendzone;

import android.app.Application;

import net.osaila.friendzone.di.AppContextModule;
import net.osaila.friendzone.di.ApplicationComponent;
import net.osaila.friendzone.di.DaggerApplicationComponent;
import net.osaila.friendzone.di.DaggerComponentProvider;

public class FriendzoneApplication extends Application implements DaggerComponentProvider {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .appContextModule(new AppContextModule(this))
                .build();
    }

    @Override
    public ApplicationComponent injector() {
        return applicationComponent;
    }
}
