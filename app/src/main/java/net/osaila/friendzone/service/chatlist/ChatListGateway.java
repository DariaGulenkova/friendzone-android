package net.osaila.friendzone.service.chatlist;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.service.login.AuthenticationException;
import net.osaila.friendzone.service.login.Principal;
import net.osaila.friendzone.service.login.db.AppDatabase;
import net.osaila.friendzone.service.login.db.PrincipalDao;

import java.io.IOException;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ChatListGateway {

    private ChatEndpoint chatEndpoint;

    private PrincipalDao principalDao;

    private MutableLiveData<Set<Chat>> chatlistData;

    @Inject
    public ChatListGateway(ChatEndpoint chatEndpoint, AppDatabase appDatabase) {
        this.chatEndpoint = chatEndpoint;
        this.principalDao = appDatabase.principalDao();
    }

    @SuppressLint("DefaultLocale")
    public LiveData<Set<Chat>> getChats() {
        if (chatlistData == null) {
            chatlistData = new MutableLiveData<>();
            updateChatList();
        }
        return chatlistData;
    }

    public void updateChats() {
        updateChatList();
    }

    private void updateChatList() {
        //TODO add auth exceptions processing
        Principal principal = principalDao.get();
        try {
            retrofit2.Response<Set<Chat>> chatsResponse = chatEndpoint.getChatsForUser(principal.getUserId(), principal.getAccessToken()).execute();
            switch (chatsResponse.code()) {
                case 200:
                    chatlistData.postValue(chatsResponse.body());
                    break;
                case 401:
                    throw new AuthenticationException(chatsResponse.raw().body().string());
                default:
                    throw new AuthenticationException("unknown error: code " + chatsResponse.code());
            }
        } catch (IOException e) {
            throw new AuthenticationException(e);
        }
    }
}
