package net.osaila.friendzone.ui.chatlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.osaila.friendzone.R;
import net.osaila.friendzone.model.Chat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ChatsAdapter extends RecyclerView.Adapter {

    //TODO Convert to LiveData, I guess?
    private List<Chat> chats;

    public ChatsAdapter() {
        chats = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View chatRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false);
        return new ChatViewHolder(chatRow);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ChatViewHolder chatViewHolder = (ChatViewHolder) viewHolder;
        Chat chat = chats.get(position);
        chatViewHolder.render(chat);
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public void addChats(Collection<Chat> chats) {
        this.chats.clear();
        this.chats.addAll(chats);
        this.chats.sort((chat1, chat2) -> chat1.getName().compareTo(chat2.getName()));
    }
}
