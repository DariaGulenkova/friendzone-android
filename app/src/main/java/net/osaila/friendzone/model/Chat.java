package net.osaila.friendzone.model;

import java.io.Serializable;

public class Chat implements Serializable {

    private long id;

    private String name;

    private ChatUser recipient;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatUser getRecipient() {
        return recipient;
    }

    public void setRecipient(ChatUser recipient) {
        this.recipient = recipient;
    }
}
