package net.osaila.friendzone.ui.training;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.osaila.friendzone.FriendzoneApplication;
import net.osaila.friendzone.R;
import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.model.ChatUser;
import net.osaila.friendzone.model.TrainingStatus;
import net.osaila.friendzone.service.location.LocationUpdate;
import net.osaila.friendzone.service.training.TrainingUpdate;
import net.osaila.friendzone.ui.chatlist.ChatListActivity;

public class TrainingActivity extends AppCompatActivity {

    private boolean spectatorMode;

    private Chat viewedChat;

    private Chronometer timer;

    private TextView distanceView;

    private TextView speedView;

    private ImageButton stopButton;

    private TrainingViewModel trainingViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);

        spectatorMode = getIntent().getBooleanExtra("spectatorMode", false);
        if (spectatorMode) {
            viewedChat = (Chat) getIntent().getSerializableExtra("viewedChat");
            setTitle(getChatRecipientName());
        }

        FriendzoneApplication app = (FriendzoneApplication) getApplication();
        trainingViewModel = ViewModelProviders.of(this, app.injector().trainingViewModelFactory()).get(TrainingViewModel.class);

        View controlsLayout = findViewById(R.id.controlsLayout);
        if (spectatorMode) {
            controlsLayout.setVisibility(View.GONE);
        }

        timer = findViewById(R.id.timeValue);
        distanceView = findViewById(R.id.distanceValue);
        speedView = findViewById(R.id.speedValue);

        stopButton = findViewById(R.id.stopButton);
        stopButton.setOnClickListener(view -> {
            setResult(RESULT_OK);
            finish();
        });
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof TrainingMapFragment) {
            ((TrainingMapFragment) fragment).setMapReadyListener(this::onMapReady);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (spectatorMode) {
            trainingViewModel.stopUserLocationTracking();
        } else {
            trainingViewModel.stopTraining();
        }
    }

    @Override
    public void onBackPressed() {
        if (spectatorMode) {
            finish();
        } else {
            Intent intent = new Intent(this, ChatListActivity.class);
            intent.putExtra("activeTraining", true);
            startActivity(intent);
            //moveTaskToBack(true);
        }
    }

    private void onMapReady(GoogleMap map) {
        if (spectatorMode) {
            trainingViewModel.startUserLocationTracking().observe(this, new UserLocationUiUpdater(map));
            TrainingStatus trainingStatus = viewedChat.getRecipient().getLastTrainingStatus();
            LatLng latLng = new LatLng(trainingStatus.getLatitude(), trainingStatus.getLongitude());
            map.addMarker(new MarkerOptions().title(getChatRecipientName()).position(latLng));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, UserLocationUiUpdater.MAP_ZOOM));
        } else {
            trainingViewModel.getTrainingUpdates().observe(this, new TrainingUiUpdater(map));
            timer.start();
        }
    }

    private String getChatRecipientName() {
        ChatUser recipient = viewedChat.getRecipient();
        return String.format("%s %s", recipient.getFirstName(), recipient.getLastName());
    }

    private class TrainingUiUpdater implements Observer<TrainingUpdate> {

        private static final int MAP_ZOOM = 15;

        private final GoogleMap map;

        private final MarkerOptions markerOptions = new MarkerOptions().title("Вы");

        private Marker currentMarker;

        public TrainingUiUpdater(GoogleMap map) {
            this.map = map;
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onChanged(@NonNull TrainingUpdate update) {
            LocationUpdate locationUpdate = update.getLocationUpdate();
            LatLng latLng = new LatLng(locationUpdate.getLatitude(), locationUpdate.getLongitude());
            if (currentMarker != null) {
                currentMarker.setPosition(latLng);
            } else {
                currentMarker = map.addMarker(markerOptions.position(latLng));
            }
            distanceView.setText(String.valueOf(Math.round(update.getDistanceMeters())));
            speedView.setText(String.format("%.2f", locationUpdate.getSpeedKph()));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM));
        }
    }

    private class UserLocationUiUpdater implements Observer<LocationUpdate> {

        private static final int MAP_ZOOM = 15;

        private final GoogleMap map;

        private final MarkerOptions markerOptions = new MarkerOptions().title("Вы");

        private Marker currentMarker;

        public UserLocationUiUpdater(GoogleMap map) {
            this.map = map;
        }

        @Override
        public void onChanged(@NonNull LocationUpdate locationUpdate) {
            LatLng latLng = new LatLng(locationUpdate.getLatitude(), locationUpdate.getLongitude());
            if (currentMarker != null) {
                currentMarker.setPosition(latLng);
            } else {
                currentMarker = map.addMarker(markerOptions.position(latLng));
            }
        }
    }
}
