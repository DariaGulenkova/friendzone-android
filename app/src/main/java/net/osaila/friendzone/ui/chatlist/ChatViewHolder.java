package net.osaila.friendzone.ui.chatlist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import net.osaila.friendzone.R;
import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.model.ChatUser;
import net.osaila.friendzone.model.TrainingStatus;
import net.osaila.friendzone.ui.training.TrainingActivity;

public class ChatViewHolder extends RecyclerView.ViewHolder {

    private View itemView;

    private TextView chatName;
    private TextView trainingStatusText;
    private ImageButton mapButton;

    public ChatViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        chatName = itemView.findViewById(R.id.chatName);
        trainingStatusText = itemView.findViewById(R.id.trainingStatus);
        mapButton = itemView.findViewById(R.id.mapButton);
    }

    public void render(Chat chat) {
        chatName.setText(getChatRecipientName(chat));
        setupControls(chat);
    }

    private void setupControls(Chat chat) {
        TrainingStatus trainingStatus = chat.getRecipient().getLastTrainingStatus();
        boolean isTraining = trainingStatus.isTraining();
        trainingStatusText.setVisibility(isTraining ? View.VISIBLE : View.INVISIBLE);
        mapButton.setVisibility(isTraining ? View.VISIBLE : View.INVISIBLE);
        mapButton.setActivated(trainingStatus.getLatitude() != 0.0);
        mapButton.setOnClickListener(view -> {
            if (view.isActivated()) {
                Intent intent = new Intent(itemView.getContext(), TrainingActivity.class);
                intent.putExtra("spectatorMode", true);
                intent.putExtra("viewedChat", chat);
                itemView.getContext().startActivity(intent);
            } else {
                Toast.makeText(view.getContext(), "Местоположение недоступно", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getChatRecipientName(Chat chat) {
        ChatUser recipient = chat.getRecipient();
        return String.format("%s %s", recipient.getFirstName(), recipient.getLastName());
    }
}
