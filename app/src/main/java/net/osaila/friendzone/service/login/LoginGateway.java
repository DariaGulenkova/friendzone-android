package net.osaila.friendzone.service.login;

import net.osaila.friendzone.service.login.db.AppDatabase;
import net.osaila.friendzone.service.login.db.PrincipalDao;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Credentials;
import retrofit2.Response;

@Singleton
public class LoginGateway {

    private static final String APP_CREDS = Credentials.basic("android-app", "");

    private OauthEndpoint oauthEndpoint;

    private PrincipalDao principalDao;

    @Inject
    public LoginGateway(OauthEndpoint oauthEndpoint, AppDatabase appDatabase) {
        this.oauthEndpoint = oauthEndpoint;
        this.principalDao = appDatabase.principalDao();
    }

    public void login(String email, String password) throws AuthenticationException {
        try {
            Response<Principal> principalResponse = oauthEndpoint.getAccessToken(APP_CREDS, "password", email, password).execute();
            switch (principalResponse.code()) {
                case 200:
                    onSuccessfulLogin(principalResponse, email);
                    break;
                case 400:
                    throw new AuthenticationException("invalid app authorization: code 400");
                case 403:
                    throw new AuthenticationException("invalid app authorization: code 403");
                default:
                    throw new AuthenticationException("unknown error: code " + principalResponse.code());
            }
        } catch (IOException e) {
            throw new AuthenticationException(e);
        }
    }

    private void onSuccessfulLogin(Response<Principal> response, String email) {
        Principal principal = response.body();
        principal.setEmail(email);
        principalDao.delete();
        principalDao.insert(principal);
    }
}
