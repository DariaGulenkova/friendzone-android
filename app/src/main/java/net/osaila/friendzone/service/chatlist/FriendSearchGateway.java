package net.osaila.friendzone.service.chatlist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import net.osaila.friendzone.model.SearchUser;
import net.osaila.friendzone.service.login.AuthenticationException;
import net.osaila.friendzone.service.login.Principal;
import net.osaila.friendzone.service.login.db.AppDatabase;
import net.osaila.friendzone.service.login.db.PrincipalDao;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Response;

@Singleton
public class FriendSearchGateway {

    private ChatEndpoint chatEndpoint;

    private FriendSearchEndpoint searchEndpoint;

    private PrincipalDao principalDao;

    @Inject
    public FriendSearchGateway(ChatEndpoint chatEndpoint, FriendSearchEndpoint searchEndpoint, AppDatabase appDatabase) {
        this.chatEndpoint = chatEndpoint;
        this.searchEndpoint = searchEndpoint;
        this.principalDao = appDatabase.principalDao();
    }

    public List<SearchUser> getUsers(String pattern) {
        Principal principal = principalDao.get();
        try {
            Response<List<SearchUser>> userResponse = searchEndpoint.getUsersByPattern(pattern, principal.getAccessToken()).execute();
            switch (userResponse.code()) {
                case 200:
                    return userResponse.body();
                case 401:
                    throw new AuthenticationException(userResponse.raw().body().string());
                default:
                    throw new AuthenticationException("unknown error: code " + userResponse.code());
            }
        } catch (IOException e) {
            throw new AuthenticationException(e);
        }
    }

    public void addFriend(long friendId) {
        Principal principal = principalDao.get();
        try {
            Response<Void> response = chatEndpoint.addFriendToUser(principal.getUserId(), friendId, principal.getAccessToken()).execute();
            if (response.code() != 200) {
                throw new AuthenticationException("add friend failed: code " + response.code());
            }
        } catch (IOException e) {
            throw new AuthenticationException(e);
        }
    }
}
