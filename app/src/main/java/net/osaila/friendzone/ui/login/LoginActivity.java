package net.osaila.friendzone.ui.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.osaila.friendzone.FriendzoneApplication;
import net.osaila.friendzone.R;
import net.osaila.friendzone.ui.chatlist.ChatListActivity;

import java.util.Optional;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEdit;
    private EditText passwordEdit;
    private Button loginButton;
    private ProgressBar loadingBar;

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FriendzoneApplication app = (FriendzoneApplication) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this, app.injector().loginViewModelFactory()).get(LoginViewModel.class);

        emailEdit = findViewById(R.id.email);
        passwordEdit = findViewById(R.id.password);
        loginButton = findViewById(R.id.login);
        loadingBar = findViewById(R.id.loading);
        setControlsAppearance(true);

        loginViewModel.getSavedLogin().observe(this, this::setSavedLogin);

        loginViewModel.getLoginState().observe(this, loginResult -> {
            if (loginResult != null) {
                setControlsAppearance(true);
                if (loginResult.getError() != null) {
                    Toast.makeText(getApplicationContext(), loginResult.getError(), Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(this, ChatListActivity.class));
                    finish();
                }
            }
        });


        loginButton.setOnClickListener(view -> {
            setControlsAppearance(false);
            loginViewModel.login(emailEdit.getText().toString(), passwordEdit.getText().toString());
        });
    }

    private void setControlsAppearance(boolean idle) {
        loadingBar.setVisibility(idle ? View.GONE : View.VISIBLE);
        emailEdit.setEnabled(idle);
        passwordEdit.setEnabled(idle);
        loginButton.setEnabled(idle);
    }

    private void setSavedLogin(Optional<String> savedLogin) {
        savedLogin.ifPresent(value -> emailEdit.setText(value));
    }
}
