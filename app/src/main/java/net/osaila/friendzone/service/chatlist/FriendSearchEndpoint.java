package net.osaila.friendzone.service.chatlist;

import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.model.SearchUser;

import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FriendSearchEndpoint {

    @GET("/users/list")
    Call<List<SearchUser>> getUsersByPattern(
            @Query("pattern") String pattern,
            @Query("access_token") String accessToken);
}
