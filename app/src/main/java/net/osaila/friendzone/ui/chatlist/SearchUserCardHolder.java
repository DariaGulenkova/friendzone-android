package net.osaila.friendzone.ui.chatlist;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.osaila.friendzone.R;
import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.model.ChatUser;
import net.osaila.friendzone.model.SearchUser;
import net.osaila.friendzone.model.TrainingStatus;
import net.osaila.friendzone.ui.training.TrainingActivity;

public class SearchUserCardHolder extends RecyclerView.ViewHolder {

    private View itemView;

    private FriendSearchViewModel friendSearchViewModel;

    private TextView userFullName;
    private TextView userEmail;
    private Button addButton;

    public SearchUserCardHolder(@NonNull View itemView, FriendSearchViewModel friendSearchViewModel) {
        super(itemView);
        this.itemView = itemView;
        this.friendSearchViewModel = friendSearchViewModel;

        userFullName = itemView.findViewById(R.id.userFullName);
        userEmail = itemView.findViewById(R.id.email);
        addButton = itemView.findViewById(R.id.addFriendButton);
    }

    public void render(SearchUser user) {
        userFullName.setText(getUserFullName(user));
        userEmail.setText(user.getEmail());
        setupControls(user);
    }

    private void setupControls(SearchUser user) {
        addButton.setOnClickListener(view -> {
            friendSearchViewModel.addFriend(user);
        });
        Observer<AddFriendResult> friendResultObserver = new Observer<AddFriendResult>() {
            @Override
            public void onChanged(@Nullable AddFriendResult addFriendResult) {
                if (addFriendResult != null) {
                    if (addFriendResult.getError() != null) {
                        Toast.makeText(itemView.getContext().getApplicationContext(), addFriendResult.getError(), Toast.LENGTH_LONG).show();
                    } else {
                        addButton.setActivated(false);
                        addButton.setText("Друг добавлен!");
                        friendSearchViewModel.getAddFriendState().removeObserver(this);
                    }
                }
            }
        };
        friendSearchViewModel.getAddFriendState().observeForever(friendResultObserver);
    }

    private String getUserFullName(SearchUser user) {
        return String.format("%s %s", user.getFirstName(), user.getLastName());
    }
}
