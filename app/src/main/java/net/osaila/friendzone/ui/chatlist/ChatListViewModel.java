package net.osaila.friendzone.ui.chatlist;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.service.chatlist.ChatListGateway;

import java.util.Set;

import javax.inject.Inject;

public class ChatListViewModel extends AndroidViewModel {

    private ChatListGateway gateway;

    private MediatorLiveData<Set<Chat>> chatlistData;

    @Inject
    public ChatListViewModel(@NonNull Application application) {
        super(application);
        chatlistData = new MediatorLiveData<>();
    }

    @Inject
    public void setGateway(ChatListGateway gateway) {
        this.gateway = gateway;
        subscribeToGateway();
    }

    public LiveData<Set<Chat>> getChats() {
        return chatlistData;
    }

    @SuppressLint("StaticFieldLeak")
    public void refreshChats() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                gateway.updateChats();
                return null;
            }
        }.execute();
    }

    private void subscribeToGateway() {
        new SubscribeTask().execute();
    }

    private class SubscribeTask extends AsyncTask<Void, Void, LiveData<Set<Chat>>> {

        @Override
        protected LiveData<Set<Chat>> doInBackground(Void... voids) {
            return gateway.getChats();
        }

        @Override
        protected void onPostExecute(LiveData<Set<Chat>> setLiveData) {
            chatlistData.addSource(gateway.getChats(), chats -> chatlistData.setValue(chats));
        }
    }
}
