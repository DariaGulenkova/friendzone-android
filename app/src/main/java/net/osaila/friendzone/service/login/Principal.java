package net.osaila.friendzone.service.login;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "principals")
public class Principal {

    @SerializedName("userId")
    @PrimaryKey
    @ColumnInfo(name = "user_id")
    @NonNull
    private Long userId;

    @ColumnInfo(name = "email")
    @NonNull
    private String email;

    @SerializedName("access_token")
    @ColumnInfo(name = "access_token")
    @NonNull
    private String accessToken;

    @SerializedName("refresh_token")
    @ColumnInfo(name = "refresh_token")
    @NonNull
    private String refreshToken;

    public Principal(@NonNull Long userId) {
        this.userId = userId;
    }

    @NonNull
    public Long getUserId() {
        return userId;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    @NonNull
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(@NonNull String accessToken) {
        this.accessToken = accessToken;
    }

    @NonNull
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(@NonNull String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
