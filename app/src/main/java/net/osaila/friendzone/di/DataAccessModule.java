package net.osaila.friendzone.di;

import android.content.Context;

import net.osaila.friendzone.service.chatlist.ChatEndpoint;
import net.osaila.friendzone.service.chatlist.FriendSearchEndpoint;
import net.osaila.friendzone.service.login.OauthEndpoint;
import net.osaila.friendzone.service.login.db.AppDatabase;
import net.osaila.friendzone.service.training.TrainingUpdateEndpoint;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataAccessModule {

    private static final String SERVER_HOST_PREFIX = "http://192.168.1.103";

    private static Retrofit authServerRetrofit = new Retrofit.Builder()
            .baseUrl(SERVER_HOST_PREFIX + ":6666")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static Retrofit appServerRetrofit = new Retrofit.Builder()
            .baseUrl(SERVER_HOST_PREFIX + ":8888")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Provides
    @Singleton
    public static OkHttpClient okHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    public static OauthEndpoint oauthEndpoint() {
        return authServerRetrofit.create(OauthEndpoint.class);
    }

    @Provides
    @Singleton
    public static ChatEndpoint chatEndpoint() {
        return appServerRetrofit.create(ChatEndpoint.class);
    }

    @Provides
    @Singleton
    public static FriendSearchEndpoint friendSearchEndpoint() {
        return appServerRetrofit.create(FriendSearchEndpoint.class);
    }

    @Provides
    @Singleton
    public static TrainingUpdateEndpoint trainingUpdateEndpoint() {
        return appServerRetrofit.create(TrainingUpdateEndpoint.class);
    }

    @Provides
    @Singleton
    public static AppDatabase appDatabase(Context context) {
        return AppDatabase.getDatabase(context);
    }
}
