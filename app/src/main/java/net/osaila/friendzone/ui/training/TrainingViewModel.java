package net.osaila.friendzone.ui.training;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import net.osaila.friendzone.service.location.LocationTrackingService;
import net.osaila.friendzone.service.location.LocationUpdate;
import net.osaila.friendzone.service.training.TrainingManagementService;
import net.osaila.friendzone.service.training.TrainingUpdate;

import javax.inject.Inject;

public class TrainingViewModel extends AndroidViewModel {

    private TrainingManagementService trainingManagementService;

    private LocationTrackingService userLocationTrackingService;

    @Inject
    public TrainingViewModel(@NonNull Application application) {
        super(application);
    }

    @Inject
    public void setTrainingManagementService(TrainingManagementService trainingManagementService) {
        this.trainingManagementService = trainingManagementService;
    }

    @Inject
    public void setUserLocationTrackingService(LocationTrackingService userLocationTrackingService) {
        this.userLocationTrackingService = userLocationTrackingService;
    }

    public LiveData<TrainingUpdate> getTrainingUpdates() {
        return trainingManagementService.start();
    }

    public void stopTraining() {
        trainingManagementService.stop();
    }

    public LiveData<LocationUpdate> startUserLocationTracking() {
        return userLocationTrackingService.start();
    }

    public void stopUserLocationTracking() {
        userLocationTrackingService.stop();
    }
}
