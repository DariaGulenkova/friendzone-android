package net.osaila.friendzone.ui.login;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import net.osaila.friendzone.R;
import net.osaila.friendzone.service.login.AuthenticationException;
import net.osaila.friendzone.service.login.LoginGateway;
import net.osaila.friendzone.service.login.Principal;
import net.osaila.friendzone.service.login.db.AppDatabase;
import net.osaila.friendzone.service.login.db.PrincipalDao;

import java.util.Optional;

import javax.inject.Inject;

public class LoginViewModel extends AndroidViewModel {

    private LoginGateway loginGateway;

    private PrincipalDao principalDao;

    private MutableLiveData<LoginResult> loginState = new MutableLiveData<>();

    private MutableLiveData<Optional<String>> savedLoginData = new MutableLiveData<>();

    @Inject
    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    @Inject
    public void setLoginGateway(LoginGateway loginGateway) {
        this.loginGateway = loginGateway;
    }

    @Inject
    public void setPrincipalDao(AppDatabase appDatabase) {
        this.principalDao = appDatabase.principalDao();
    }

    public LiveData<Optional<String>> getSavedLogin() {
        new LoginRetrievalTask().execute();
        return savedLoginData;
    }

    public void login(String email, String password) {
        if (email.trim().isEmpty()) {
            loginState.setValue(new LoginResult(R.string.invalid_email));
        } else if (password.trim().isEmpty()) {
            loginState.setValue(new LoginResult(R.string.invalid_password));
        } else {
            new LoginTask(email, password).execute();
        }
    }

    public LiveData<LoginResult> getLoginState() {
        return loginState;
    }

    private class LoginRetrievalTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Principal principal = principalDao.get();
            Optional<String> result = principal == null ? Optional.empty() : Optional.of(principal.getEmail());
            savedLoginData.postValue(result);
            return null;
        }


    }

    private class LoginTask extends AsyncTask<Void, Void, Void> {

        private String email;

        private String password;

        private AuthenticationException error;

        LoginTask(String email, String password) {
            this.email = email;
            this.password = password;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                loginGateway.login(email.trim(), password.trim());
            } catch (AuthenticationException e) {
                error = e;
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            if (error != null) {
                loginState.setValue(new LoginResult(R.string.login_failed));
            } else {
                loginState.setValue(new LoginResult());
            }
        }
    }
}
