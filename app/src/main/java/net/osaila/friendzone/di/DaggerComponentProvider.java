package net.osaila.friendzone.di;

public interface DaggerComponentProvider {

    ApplicationComponent injector();
}
