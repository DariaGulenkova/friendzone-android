package net.osaila.friendzone.service.training;

import net.osaila.friendzone.service.login.AuthenticationException;
import net.osaila.friendzone.service.login.Principal;
import net.osaila.friendzone.service.login.db.AppDatabase;
import net.osaila.friendzone.service.login.db.PrincipalDao;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrainingUpdateSendingService {

    private TrainingUpdateEndpoint updateEndpoint;

    private PrincipalDao principalDao;

    @Inject
    public TrainingUpdateSendingService(TrainingUpdateEndpoint updateEndpoint, AppDatabase appDatabase) {
        this.updateEndpoint = updateEndpoint;
        this.principalDao = appDatabase.principalDao();
    }

    public void startUpdateSeries() {
        Principal principal = principalDao.get();
        updateEndpoint.sendStart(principal.getUserId(), principal.getAccessToken())
                .enqueue(new ExceptionCheckingCallback());
    }

    public void sendUpdate(TrainingUpdate update) {
        Principal principal = principalDao.get();
        updateEndpoint.sendUpdate(principal.getUserId(), update, principal.getAccessToken())
                .enqueue(new ExceptionCheckingCallback());
    }

    public void stopUpdateSeries() {
        Principal principal = principalDao.get();
        updateEndpoint.sendStop(principal.getUserId(), principal.getAccessToken())
                .enqueue(new ExceptionCheckingCallback());
    }

    private class ExceptionCheckingCallback implements Callback<Void> {
        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {
            try {
                switch (response.code()) {
                    case 200:
                        break;
                    case 401:
                        throw new AuthenticationException(response.raw().body().string());
                    default:
                        throw new AuthenticationException("unknown error: code " + response.code());
                }
            } catch (IOException e) {
                throw new AuthenticationException(e);
            }
        }

        @Override
        public void onFailure(Call<Void> call, Throwable t) {
            throw new AuthenticationException(t);
        }
    }
}
