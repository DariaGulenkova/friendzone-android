package net.osaila.friendzone.ui.chatlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.osaila.friendzone.R;
import net.osaila.friendzone.model.Chat;
import net.osaila.friendzone.model.SearchUser;
import net.osaila.friendzone.service.chatlist.ChatEndpoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SearchUsersAdapter extends RecyclerView.Adapter {

    private FriendSearchViewModel friendSearchViewModel;

    private List<SearchUser> users;

    public SearchUsersAdapter(FriendSearchViewModel friendSearchViewModel) {
        this.friendSearchViewModel = friendSearchViewModel;
        users = new ArrayList<>();
    }

    @NonNull
    @Override
    public SearchUserCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View chatRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_user_row, parent, false);
        return new SearchUserCardHolder(chatRow, friendSearchViewModel);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SearchUserCardHolder userCardHolder = (SearchUserCardHolder) viewHolder;
        SearchUser user = users.get(position);
        userCardHolder.render(user);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void addUsers(Collection<SearchUser> users) {
        this.users.clear();
        this.users.addAll(users);
    }
}
