package net.osaila.friendzone.service.chatlist;

class ChatListException extends RuntimeException {

    public ChatListException(String message) {
        super(message);
    }

    public ChatListException(Throwable cause) {
        super(cause);
    }
}
