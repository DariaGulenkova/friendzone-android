package net.osaila.friendzone.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Provider;

class DaggerViewModelFactory<VM extends ViewModel> implements ViewModelProvider.Factory {

    private Provider<VM> viewModel;

    @Inject
    public DaggerViewModelFactory(Provider<VM> viewModel) {
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) viewModel.get();
    }
}
