package net.osaila.friendzone.ui.chatlist;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.osaila.friendzone.FriendzoneApplication;
import net.osaila.friendzone.R;
import net.osaila.friendzone.ui.training.TrainingActivity;

public class ChatListActivity extends AppCompatActivity {

    private static final int TRAINING_REQUEST_CODE = 1;

    private static final int FRIEND_SEARCH_REQUEST_CODE = 2;

    private ImageView selfAvatar;
    private TextView trainingStatus;
    private ImageButton startButton;
    private ProgressBar trainingLoadingBar;

    private ProgressBar loadingBar;

    private ChatListViewModel chatListViewModel;
    private ChatsAdapter chatsAdapter;

    private SwipeRefreshLayout refreshChatsOnSwipeLayout;

    private FloatingActionButton addFriendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FriendzoneApplication app = (FriendzoneApplication) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatlist);

        trainingStatus = findViewById(R.id.trainingStatus);
        startButton = findViewById(R.id.startButton);
        trainingLoadingBar = findViewById(R.id.trainingLoadingBar);
        loadingBar = findViewById(R.id.loadingBar);

        boolean trainingIsActive = getIntent().getBooleanExtra("activeTraining", false);
        setControlsAppearance(!trainingIsActive);

        chatsAdapter = new ChatsAdapter();
        RecyclerView chatListView = findViewById(R.id.chat_list);
        chatListView.setLayoutManager(new LinearLayoutManager(this));
        chatListView.setAdapter(chatsAdapter);

        chatListViewModel = ViewModelProviders.of(this, app.injector().chatListViewModelFactory()).get(ChatListViewModel.class);

        refreshChatsOnSwipeLayout = findViewById(R.id.swipeRefreshChatLayout);
        refreshChatsOnSwipeLayout.setOnRefreshListener(chatListViewModel::refreshChats);

        chatListViewModel.getChats().observe(this, chats -> {
            loadingBar.setVisibility(View.GONE);
            chatsAdapter.addChats(chats);
            chatsAdapter.notifyDataSetChanged();

            refreshChatsOnSwipeLayout.setRefreshing(false);
        });

        startButton.setOnClickListener(view -> {
            //TODO don't start training if one is active already
            startButton.setVisibility(View.INVISIBLE);
            trainingLoadingBar.setVisibility(View.VISIBLE);

            startActivityForResult(new Intent(this, TrainingActivity.class), TRAINING_REQUEST_CODE);
            setControlsAppearance(false);
        });

        addFriendButton = findViewById(R.id.startFriendSearchButton);
        addFriendButton.setOnClickListener(view -> {
            startActivityForResult(new Intent(this, FriendSearchActivity.class), FRIEND_SEARCH_REQUEST_CODE);
        });
    }

    private void setControlsAppearance(boolean idle) {
        trainingLoadingBar.setVisibility(View.INVISIBLE);
        if (idle) {
            trainingStatus.setVisibility(View.INVISIBLE);
            startButton.setImageResource(R.drawable.ic_play_circle);
        } else {
            trainingStatus.setVisibility(View.VISIBLE);
            startButton.setImageResource(R.drawable.ic_earth);
        }
        startButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case TRAINING_REQUEST_CODE:
                setControlsAppearance(true);
                break;
            case FRIEND_SEARCH_REQUEST_CODE:
                chatListViewModel.refreshChats();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
