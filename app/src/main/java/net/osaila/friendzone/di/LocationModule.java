package net.osaila.friendzone.di;

import android.content.Context;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocationModule {

    @Provides
    public static LocationRequest locationRequest() {
        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(TimeUnit.SECONDS.toMillis(10));
        request.setFastestInterval(TimeUnit.SECONDS.toMillis(5));
        return request;
    }

    @Provides
    @Singleton
    public static FusedLocationProviderClient fusedLocationProviderClient(Context context) {
        return new FusedLocationProviderClient(context);
    }
}
