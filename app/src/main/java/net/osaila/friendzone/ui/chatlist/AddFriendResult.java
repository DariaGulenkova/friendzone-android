package net.osaila.friendzone.ui.chatlist;

public class AddFriendResult {

    private Integer error;

    public AddFriendResult() {
    }

    public AddFriendResult(int error) {
        this.error = error;
    }

    public Integer getError() {
        return error;
    }
}
