package net.osaila.friendzone.ui.chatlist;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.osaila.friendzone.FriendzoneApplication;
import net.osaila.friendzone.R;

public class FriendSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FriendzoneApplication app = (FriendzoneApplication) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_search);

        FriendSearchViewModel friendSearchViewModel = ViewModelProviders.of(this, app.injector().friendSearchViewModelFactory()).get(FriendSearchViewModel.class);
        SearchUsersAdapter usersAdapter = new SearchUsersAdapter(friendSearchViewModel);

        RecyclerView friendList = findViewById(R.id.friendList);
        friendList.setLayoutManager(new LinearLayoutManager(this));
        friendList.setAdapter(usersAdapter);

        ProgressBar loadingBar = findViewById(R.id.loadingBar);

        friendSearchViewModel.getUserData().observe(this, foundUsers -> {
            loadingBar.setVisibility(View.GONE);
            usersAdapter.addUsers(foundUsers);
            usersAdapter.notifyDataSetChanged();
        });

        EditText friendEmail = findViewById(R.id.friendEmail);
        ImageButton searchButton = findViewById(R.id.searchButton);

        searchButton.setOnClickListener(v -> {
            String email = friendEmail.getText().toString().trim();
            if (email.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Введите email друга", Toast.LENGTH_SHORT).show();
            } else {
                loadingBar.setVisibility(View.VISIBLE);
                friendSearchViewModel.findUsersByPattern(email);
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
