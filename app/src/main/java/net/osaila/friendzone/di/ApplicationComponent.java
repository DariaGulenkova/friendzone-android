package net.osaila.friendzone.di;

import net.osaila.friendzone.ui.chatlist.ChatListViewModel;
import net.osaila.friendzone.ui.chatlist.FriendSearchViewModel;
import net.osaila.friendzone.ui.login.LoginViewModel;
import net.osaila.friendzone.ui.training.TrainingViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {LocationModule.class, DataAccessModule.class, AppContextModule.class})
@Singleton
public interface ApplicationComponent {

    DaggerViewModelFactory<LoginViewModel> loginViewModelFactory();

    DaggerViewModelFactory<ChatListViewModel> chatListViewModelFactory();

    DaggerViewModelFactory<FriendSearchViewModel> friendSearchViewModelFactory();

    DaggerViewModelFactory<TrainingViewModel> trainingViewModelFactory();
}
