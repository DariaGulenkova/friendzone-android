package net.osaila.friendzone.service.location;

import android.arch.lifecycle.MutableLiveData;
import android.location.Location;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

class LocationUpdateCallback extends LocationCallback {

    private MutableLiveData<LocationUpdate> locationUpdateData;

    LocationUpdateCallback(MutableLiveData<LocationUpdate> locationUpdateData) {
        this.locationUpdateData = locationUpdateData;
    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        Location gpsLocation = locationResult.getLastLocation();
        LocationUpdate locationUpdate = new LocationUpdate();
        locationUpdate.setLatitude(gpsLocation.getLatitude());
        locationUpdate.setLongitude(gpsLocation.getLongitude());
        locationUpdate.setSpeedKph(mpsToKph(gpsLocation.getSpeed()));
        locationUpdateData.postValue(locationUpdate);
    }

    private double mpsToKph(double speedMetersPerSeconds) {
        return speedMetersPerSeconds * 3600 / 1000;
    }
}
