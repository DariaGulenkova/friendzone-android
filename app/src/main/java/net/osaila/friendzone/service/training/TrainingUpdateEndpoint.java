package net.osaila.friendzone.service.training;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TrainingUpdateEndpoint {

    @POST("/users/{userId}/training/start")
    Call<Void> sendStart(
            @Path("userId") long userId,
            @Query("access_token") String accessToken);

    @POST("/users/{userId}/training/update")
    Call<Void> sendUpdate(
            @Path("userId") long userId,
            @Body TrainingUpdate update,
            @Query("access_token") String accessToken);

    @POST("/users/{userId}/training/stop")
    Call<Void> sendStop(
            @Path("userId") long userId,
            @Query("access_token") String accessToken);
}
