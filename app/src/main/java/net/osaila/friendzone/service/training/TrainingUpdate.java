package net.osaila.friendzone.service.training;

import net.osaila.friendzone.service.location.LocationUpdate;

public class TrainingUpdate {

    private LocationUpdate locationUpdate;

    private long trainingTimeSeconds;

    private double distanceMeters;

    public LocationUpdate getLocationUpdate() {
        return locationUpdate;
    }

    public void setLocationUpdate(LocationUpdate locationUpdate) {
        this.locationUpdate = locationUpdate;
    }

    public long getTrainingTimeSeconds() {
        return trainingTimeSeconds;
    }

    public void setTrainingTimeSeconds(long trainingTimeSeconds) {
        this.trainingTimeSeconds = trainingTimeSeconds;
    }

    public double getDistanceMeters() {
        return distanceMeters;
    }

    public void setDistanceMeters(double distanceMeters) {
        this.distanceMeters = distanceMeters;
    }
}
