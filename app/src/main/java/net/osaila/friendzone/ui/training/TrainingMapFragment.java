package net.osaila.friendzone.ui.training;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import net.osaila.friendzone.R;

public class TrainingMapFragment extends Fragment {

    private MapReadyListener listener;
    //private Marker currentMarker;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FriendzoneApplication app = (FriendzoneApplication) getActivity().getApplication();
        //locationTrackingService = app.injector().locationTrackingService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map_training, container, false);
    }

    //TODO move location tracking to parent activity
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.trainingMap);
        mapFragment.getMapAsync(googleMap -> {
            if (listener != null) {
                listener.onMapReady(googleMap);
            }
        });
    }

    public void setMapReadyListener (MapReadyListener listener) {
        this.listener = listener;
    }

    //    private void startTracking() {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//            locationTrackingService.start();
//        }
//    }

//    public void stopLocationTracking() {
//        if (locationTrackingService != null) {
//            locationTrackingService.stop(locCallback);
//        }
//    }

//    private class LocCallback extends LocationCallback {
//
//        @Override
//        public void onLocationResult(LocationResult locationResult) {
//            LatLng latLng = new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());
//            if (currentMarker != null) {
//                currentMarker.setPosition(latLng);
//            } else {
//                currentMarker = map.addMarker(new MarkerOptions().position(latLng).title("Вы"));
//            }
//            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//            Toast.makeText(getContext(), String.valueOf(locationResult.getLastLocation().getSpeed()), Toast.LENGTH_LONG).show();
//        }
//    }

    public interface MapReadyListener {

        void onMapReady(GoogleMap map);
    }
}
