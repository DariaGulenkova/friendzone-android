package net.osaila.friendzone.model;

import java.io.Serializable;

public class ChatUser implements Serializable {

    private long userId;

    private String firstName;

    private String lastName;

    private boolean active;

    private TrainingStatus lastTrainingStatus;

    public long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public TrainingStatus getLastTrainingStatus() {
        return lastTrainingStatus;
    }

    public void setLastTrainingStatus(TrainingStatus lastTrainingStatus) {
        this.lastTrainingStatus = lastTrainingStatus;
    }
}
