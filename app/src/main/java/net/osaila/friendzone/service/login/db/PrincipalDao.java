package net.osaila.friendzone.service.login.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import net.osaila.friendzone.service.login.Principal;

@Dao
public interface PrincipalDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Principal principal);

    @Query("SELECT * FROM principals LIMIT 1")
    Principal get();

    @Query("DELETE FROM principals")
    void delete();
}
