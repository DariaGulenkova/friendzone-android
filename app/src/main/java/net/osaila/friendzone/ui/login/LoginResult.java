package net.osaila.friendzone.ui.login;

public class LoginResult {

    private Integer error;

    public LoginResult() {
    }

    public LoginResult(int error) {
        this.error = error;
    }

    public Integer getError() {
        return error;
    }
}
