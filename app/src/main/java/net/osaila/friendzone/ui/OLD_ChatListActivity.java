package net.osaila.friendzone.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.here.oksse.OkSse;
import com.here.oksse.ServerSentEvent;

import net.osaila.friendzone.R;

import okhttp3.Credentials;
import okhttp3.Request;
import okhttp3.Response;

public class OLD_ChatListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatlist);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public void subscribe(View subscribeButton) {
        final EditText eventsText = null;//findViewById(R.id.eventsList);
        final Handler handler = new Handler();

        new OkSse().newServerSentEvent(
                new Request.Builder()
                        .url("http://192.168.1.104:8095/chatting/events")
                        .header("Authorization", Credentials.basic("alpha", "a_password"))
                        .build(), new ServerSentEvent.Listener() {
            @Override
            public void onOpen(ServerSentEvent sse, Response response) {
                System.err.println("wowser");
            }

            @WorkerThread
            @Override
            public void onMessage(ServerSentEvent sse, String id, String event, final String message) {
                handler.post(() -> {
                    eventsText.getText().append(message + '\n');
                    eventsText.invalidate();
                });
            }

            @Override
            public void onComment(ServerSentEvent sse, String comment) {
                System.err.println("comment");
            }

            @Override
            public boolean onRetryTime(ServerSentEvent sse, long milliseconds) {
                return false;
            }

            @Override
            public boolean onRetryError(ServerSentEvent sse, Throwable throwable, Response response) {
                return false;
            }

            @Override
            public void onClosed(ServerSentEvent sse) {
                System.err.println("wtf");
            }

            @Override
            public Request onPreRetry(ServerSentEvent sse, Request originalRequest) {
                return null;
            }
        });
    }
}
