package net.osaila.friendzone.service.training;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;

import net.osaila.friendzone.service.location.LocationTrackingService;
import net.osaila.friendzone.service.location.LocationUpdate;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

public class TrainingManagementService {

    private static final long UPDATE_SENDING_DELAY = TimeUnit.SECONDS.toMillis(10);

    private TrainingUpdateSendingService updateSendingService;

    private ScheduledExecutorService updateTaskExecutor;

    private LocationTrackingService locationTrackingService;

    private AtomicReference<TrainingUpdate> lastUpdate;

    private double currentDistanceMeters;

    private long startTimeMillis;

    @Inject
    public TrainingManagementService() {
        updateTaskExecutor = Executors.newSingleThreadScheduledExecutor();
        lastUpdate = new AtomicReference<>();
    }

    @Inject
    public void setUpdateSendingService(TrainingUpdateSendingService updateSendingService) {
        this.updateSendingService = updateSendingService;
    }

    @Inject
    public void setLocationTrackingService(LocationTrackingService locationTrackingService) {
        this.locationTrackingService = locationTrackingService;
    }

    public LiveData<TrainingUpdate> start() {
        lastUpdate = new AtomicReference<>();
        currentDistanceMeters = 0;
        startTimeMillis = System.currentTimeMillis();

        Executors.newSingleThreadExecutor().execute(() -> updateSendingService.startUpdateSeries());
        updateTaskExecutor.scheduleWithFixedDelay(this::sendUpdate, UPDATE_SENDING_DELAY, UPDATE_SENDING_DELAY, TimeUnit.MILLISECONDS);
        return Transformations.map(locationTrackingService.start(), this::processLocationUpdate);
    }


    public void stop() {
        updateTaskExecutor.shutdownNow();
        Executors.newSingleThreadExecutor().execute(() -> updateSendingService.stopUpdateSeries());
        locationTrackingService.stop();
    }

    private void sendUpdate() {
        if (lastUpdate.get() != null) {
            updateSendingService.sendUpdate(lastUpdate.get());
        }
    }

    private TrainingUpdate processLocationUpdate(LocationUpdate locationUpdate) {
        TrainingUpdate trainingUpdate = new TrainingUpdate();
        trainingUpdate.setLocationUpdate(locationUpdate);
        trainingUpdate.setDistanceMeters(calculateDistance(locationUpdate));
        trainingUpdate.setTrainingTimeSeconds(calculateElapsedTime());
        lastUpdate.set(trainingUpdate);
        return trainingUpdate;
    }

    private double calculateDistance(LocationUpdate currentLocation) {
        TrainingUpdate lastUpdateValue = lastUpdate.get();
        long distanceBetweenPointsMeters = 0;
        if (lastUpdateValue != null && currentLocation.getSpeedKph() != 0) {
            LocationUpdate lastLocation = lastUpdateValue.getLocationUpdate();
            double lastLat = lastLocation.getLatitude();
            double lastLng = lastLocation.getLongitude();
            double currentLat = currentLocation.getLatitude();
            double currentLng = currentLocation.getLongitude();

            double dLat = Math.toRadians(currentLat - lastLat);
            double dLon = Math.toRadians(currentLng - lastLng);
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                    + Math.cos(Math.toRadians(lastLat))
                    * Math.cos(Math.toRadians(currentLat)) * Math.sin(dLon / 2)
                    * Math.sin(dLon / 2);
            double c = 2 * Math.asin(Math.sqrt(a));
            distanceBetweenPointsMeters = Math.round(6371000 * c);
        }
        return currentDistanceMeters += distanceBetweenPointsMeters;
    }

    private long calculateElapsedTime() {
        return (System.currentTimeMillis() - startTimeMillis) / 1000;
    }
}
