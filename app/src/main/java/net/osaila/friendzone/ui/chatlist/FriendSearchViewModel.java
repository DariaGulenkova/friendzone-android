package net.osaila.friendzone.ui.chatlist;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import net.osaila.friendzone.R;
import net.osaila.friendzone.model.SearchUser;
import net.osaila.friendzone.service.chatlist.FriendSearchGateway;
import net.osaila.friendzone.service.login.AuthenticationException;

import java.util.List;

import javax.inject.Inject;

public class FriendSearchViewModel extends AndroidViewModel {

    private FriendSearchGateway gateway;

    private MutableLiveData<List<SearchUser>> userData;

    private MutableLiveData<AddFriendResult> addFriendState = new MutableLiveData<>();

    @Inject
    public FriendSearchViewModel(@NonNull Application application) {
        super(application);
        userData = new MutableLiveData<>();
    }

    @Inject
    public void setGateway(FriendSearchGateway gateway) {
        this.gateway = gateway;
    }

    public LiveData<List<SearchUser>> getUserData() {
        return userData;
    }

    public void findUsersByPattern(String pattern) {
        new FriendSearchTask(pattern).execute();
    }

    public LiveData<AddFriendResult> getAddFriendState() {
        return addFriendState;
    }

    public void addFriend(SearchUser friend) {
        new AddFriendTask(friend.getId()).execute();
    }

    private class FriendSearchTask extends AsyncTask<Void, Void, Void> {

        private final String pattern;

        private List<SearchUser> users;

        public FriendSearchTask(String pattern) {
            this.pattern = pattern;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            users = gateway.getUsers(pattern);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            userData.setValue(users);
        }
    }

    private class AddFriendTask extends AsyncTask<Void, Void, Void> {

        private long friendId;

        private AuthenticationException error;

        public AddFriendTask(long friendId) {
            this.friendId = friendId;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                gateway.addFriend(friendId);
            } catch (AuthenticationException e) {
                error = e;
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            if (error != null) {
                addFriendState.setValue(new AddFriendResult(R.string.add_friend_failed));
            } else {
                addFriendState.setValue(new AddFriendResult());
            }
        }
    }
}
