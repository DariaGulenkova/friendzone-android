package net.osaila.friendzone.service.chatlist;

import net.osaila.friendzone.model.Chat;

import java.util.Set;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ChatEndpoint {

    @GET("/users/{userId}/chats")
    Call<Set<Chat>> getChatsForUser(
            @Path("userId") long userId,
            @Query("access_token") String accessToken);

    @POST("/users/{userId}/chats/add")
    Call<Void> addFriendToUser(
            @Path("userId") long userId,
            @Body long friendId,
            @Query("access_token") String accessToken);
}
