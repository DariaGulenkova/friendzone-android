package net.osaila.friendzone.service.login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface OauthEndpoint {

    @FormUrlEncoded
    @POST("/oauth/token")
    Call<Principal> getAccessToken(
            @Header("Authorization") String authorization,
            @Field("grant_type") String grantType,
            @Field("username") String username,
            @Field("password") String password);
}
