package net.osaila.friendzone.di;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class AppContextModule {

    private Application application;

    public AppContextModule(Application application) {
        this.application = application;
    }

    @Provides
    public Application application() {
        return application;
    }

    @Provides
    public Context appContext() {
        return application.getApplicationContext();
    }
}
