package net.osaila.friendzone.service.location;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LocationTrackingService {

    private FusedLocationProviderClient locationProviderClient;

    private LocationRequest locationRequest;

    private LocationCallback locationCallback;

    @Inject
    public LocationTrackingService(FusedLocationProviderClient locationProviderClient, LocationRequest locationRequest) {
        this.locationProviderClient = locationProviderClient;
        this.locationRequest = locationRequest;
    }

    @SuppressLint("MissingPermission")
    public LiveData<LocationUpdate> start() {
        MutableLiveData<LocationUpdate> liveData = new MutableLiveData<>();
        locationCallback = new LocationUpdateCallback(liveData);
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
        return liveData;
    }

    public void stop() {
        if (locationCallback != null) {
            locationProviderClient.removeLocationUpdates(locationCallback);
            locationCallback = null;
        }
    }
}
